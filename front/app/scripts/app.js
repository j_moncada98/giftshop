'use strict';

/**
 * @ngdoc overview
 * @name giftShopApp
 * @description
 * # giftShopApp
 *
 * Main module of the application.
 */
angular.module('giftShopApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ngTable'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'loginCtrl',
      })
      .when('/addProduct', {
        templateUrl: 'views/product.html',
        controller: 'productCtrl',
        controllerAs: 'addProduct'
      })
      .otherwise({
        redirectTo: '/'
      });
  }).config(['$httpProvider', function ($httpProvider) {

    $httpProvider.interceptors.push(function ($q,authService) {
        return {
            'responseError': function (rejection) {
                if (rejection.status === 401) {
                    authService.deleteDataToken();
                    window.location = window.appRoot + '#!/login';
                } else {
                    return $q.reject(rejection);
                }
            },
            'request':function(req){
                if(req.url.indexOf('views')>=0 && req.url.indexOf('login') < 0 && !authService.getDataToken()){
                    window.location = window.appRoot + '#!/login';
                }else if(req.url.indexOf('login') >= 0 && authService.getDataToken()){
                    window.location = window.appRoot;
                }

                if(authService.getDataToken()){
                    req.headers['Authorization'] ='Basic '+authService.getDataToken();
                }

                return req;
            },
            'response': function (response) {
                if (response.config.url.indexOf(window.apiREST + '/login') >= 0||(response.data && 'string'===typeof(response.data) &&response.data.indexOf("<form name='f' action='/login' method='POST'>") >= 0)) {
                    localStorage.setItem('username', '');
                    window.location = window.appRoot + '#!/login';
                }
                return response;
            }
        };
    });


}]).constant('CONFIG', {
  APLICACION_ID: 'Basic Z2lmdHNob3A6MTIzNDU2'
});

  window.apiREST = 'http://localhost:8080/';
  window.appRoot = 'http://localhost:9000/';
