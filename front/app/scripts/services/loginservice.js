'use strict';

/**
 * @ngdoc service
 * @name giftShopApp.loginService
 * @description
 * # loginService
 * Service in the giftShopApp.
 */
angular.module('giftShopApp')
.service('loginService',['$http','$rootScope','CONFIG','authService', 
function($http,$rootScope,CONFIG,authService){

this.getToken = function(credentials){
  return $http({
    url: window.apiREST+'user/data',
    method: "GET",
    ///data:$.param( { username: user.username, password:user.password, grant_type:'password'}),
    headers: {'Content-Type': 'application/x-www-form-urlencoded',Authorization: "Basic "+ btoa(credentials.username + ":" + credentials.password)}
  });
};

this.revokeToken = function(){
  return $http({
    url: window.apiREST+'user/oauth/token/revoke',
    method: "POST",
    data:$.param( { token: authService.getDataToken().access_token}),
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  });
};

}]);
