'use strict';

/**
 * @ngdoc service
 * @name giftShopApp.authService
 * @description
 * # authService
 * Service in the giftShopApp.
 */
angular.module('giftShopApp')
.service('authService', ['$rootScope','$cookieStore',function ($rootScope,$cookieStore) {
  this.setDataToken = function(dataToken){
    $cookieStore.put('dataToken',dataToken,{'expires':'40000','path':'\\'});
  };

  this.getDataToken = function(){
    return $cookieStore.get('dataToken');
  };

  this.deleteDataToken = function(){
    $cookieStore.remove('dataToken');
  };

  this.setDataUser = function(dataUser){
    $cookieStore.put('dataUser',dataUser,{'expires':'40000','path':'\\'});
  };

  this.getDataUser = function(){
    return $cookieStore.get('dataUser');
  };

  this.deleteDataUser = function(){
      $cookieStore.remove('dataUser');
   };

   this.setUserAuthenticated=function(user){
    $cookieStore.put('userAuthenticated',user);
   };

   this.getUserAuthenticated=function(){
    return $cookieStore.get('userAuthenticated');
   };

}]);