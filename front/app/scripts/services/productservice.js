'use strict';

/**
 * @ngdoc service
 * @name giftShopApp.productService
 * @description
 * # productService
 * Service in the giftShopApp.
 */
angular.module('giftShopApp')
  .service('productService', function ($http) {
    //$http.defaults.headers.common.Authorization = "Basic dXNlcjpob2xhMTIz";
	  //$http.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
    //var api = window.apiREST + 'home/';

    /*this.getNombreUsuario = function () {
     return $http.get(window.apiREST + 'usuario/getNombreUsuario');
    }; */

    this.getProductsPageable = function (idCategory,params){
    	return $http.get(window.apiREST + 'product/list/'+idCategory,{
                params: params
            });
    };

    this.save = function (product){
    	return $http.post(window.apiREST + 'product',product);
    };

    this.delete = function (id){
    	return $http.delete(window.apiREST + 'product/'+id);
    };



  });
