'use strict';

/**
 * @ngdoc function
 * @name giftShopApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the giftShopApp
 */
angular.module('giftShopApp')
.controller('MainCtrl', function ($scope,$rootScope,loginService,authService,CONFIG,NgTableParams,productService) {
  $scope.images = window.appRoot;
  $scope.userName = "default"
  $scope.isLoginPage = function(){
    
    if(window.location.href.indexOf('/login') >= 0){
      return true;
    }
    $scope.userName = authService.getDataUser().name;
    return false;
  };

  $rootScope.logout = function(){
    authService.deleteDataToken();
    authService.setUserAuthenticated(false);
    authService.deleteDataUser();
    window.location = window.appRoot + '#!/login';
  };

  $scope.params = {text:''};
  
  var search = function () {
      $scope.gridProducts.reload();
      $scope.gridProducts = createGridOptions();
  }


  $scope.gridProducts = createGridOptions();
    function createGridOptions() {
      var initialParams = {
          count: 10
      };
      var initialSettings = {
          counts: 10,
          paginationMaxBlocks: 13,
          paginationMinBlocks: 2,
          getData: function ($defer, params) {
              $scope.params.page = params.page() - 1;
              $scope.params.size = params.count();
              productService.getProductsPageable(0,$scope.params)
                  .then(function (res) {
                      params.total(res.data.totalElements);
                      $defer.resolve(res.data.content);
                      if (res.data.totalElements / res.data.size != res.data.totalPages) {
                          $scope.params.page = 1;
                      };
                  }, function (reason) {
                      $defer.reject();
                  });
          }
      };
      return new NgTableParams(initialParams, initialSettings);  
  };

  $rootScope.delete = function(id){
    productService.delete(id).then(function (res) {
      $scope.gridProducts.reload();
      $scope.gridProducts = createGridOptions();
    }, function (reason) {
        $defer.reject();
    });
  };

  $scope.isAdmin = function(){
    if(authService.getDataUser() && authService.getDataUser().authorities){
      for(var i = 0; i < authService.getDataUser().authorities.length;i++){
        if(authService.getDataUser().authorities[i].authority === "administrator"){
          return false;
        }
      }
    }

    return true;
  }


});
