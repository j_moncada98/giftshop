'use strict';

/**
 * @ngdoc function
 * @name giftShopApp.controller:ProductCtrl
 * @description
 * # ProductCtrl
 * Controller of the giftShopApp
 */
angular.module('giftShopApp')
  .controller('productCtrl', function ($scope,productService) {
    $scope.product={
      name:'',
      description:'',
      quantity:0
    };


    $scope.save = function(){
      productService.save($scope.product).then(function(result){
        $scope.product={
          name:'',
          description:'',
          quantity:0
        };
    
      },function(){
          $scope.showError=true;
          $timeout(function(){$scope.showError=false;},2000);
      });
    };
    


  });
