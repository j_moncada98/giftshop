'use strict';

/**
 * @ngdoc function
 * @name giftShopApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the giftShopApp
 */
angular.module('giftShopApp')
.controller('loginCtrl',['$scope','$location','$timeout','loginService','authService',
function ($scope,$location,$timeout, loginService,authService) {
$scope.userLogin={
  username:'',
  password:''
};

$scope.showError = false;
$scope.showSuccess = false;


$scope.message="";

$scope.login = function(){
  loginService.getToken($scope.userLogin).then(function(result){
    authService.setUserAuthenticated(true);
    authService.setDataToken(btoa($scope.userLogin.username + ":" + $scope.userLogin.password));
    authService.setDataUser(result.data);
    $scope.showSuccess=true;
    $timeout(function(){$scope.showSuccess=false; $location.path('/');},2000);

  },function(){
      $scope.showError=true;
      $timeout(function(){$scope.showError=false;},2000);
  });
};


}]);
