'use strict';

/**
 * @ngdoc function
 * @name giftShopApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the giftShopApp
 */
angular.module('giftShopApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
