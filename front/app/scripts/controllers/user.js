'use strict';

/**
 * @ngdoc function
 * @name giftShopApp.controller:UserCtrl
 * @description
 * # UserCtrl
 * Controller of the giftShopApp
 */
angular.module('giftShopApp')
  .controller('UserCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
