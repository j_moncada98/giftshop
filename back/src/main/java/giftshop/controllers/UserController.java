package giftshop.controllers;

import org.springframework.web.bind.annotation.*;

import java.security.Principal;

/**
 * Created by jmoncada on 25/03/2018.
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @GetMapping(value = "/data")
    public Principal user(Principal user ){
        return user;
    }
}
