package giftshop.controllers;

import giftshop.entities.Product;
import giftshop.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.Principal;

/**
 * Created by jmoncada on 26/03/2018.
 */
@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping(value= "list/{idCategory}")
    public HttpEntity<Page<Product>> getProducts(@PathVariable("idCategory") Long idCategory, Pageable pageable) {
        Page<Product> page = productService.getProductsPageable(idCategory == 0? null : idCategory,pageable);
        return new ResponseEntity<Page<Product>>(page, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity saveOrUpdate(@RequestBody Product product) {
        this.productService.saveOrUpdate(product);
        return new ResponseEntity(true, HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public HttpEntity delete(@PathVariable("id") long id, Pageable pageable) {
        productService.delete(id);
        return new ResponseEntity(true, HttpStatus.OK);
    }
}
