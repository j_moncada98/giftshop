package giftshop.services;

import giftshop.entities.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by jmoncada on 26/03/2018.
 */
public interface ProductService {

    Page<Product> getProductsPageable(Long idCategory, Pageable pageable);

    void saveOrUpdate(Product product);

    void delete(Long id);
}
