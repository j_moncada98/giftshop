package giftshop.repositories;

import giftshop.entities.User;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by jmoncada on 25/03/2018.
 */
public interface UserRepository extends CrudRepository<User,Long> {
    User findByUserName(String userName);
}
