package giftshop.repositories;

import giftshop.entities.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 * Created by jmoncada on 26/03/2018.
 */
public interface ProductRepository extends CrudRepository<Product,Long> {
    @Query(value="SELECT p " +
            "FROM Product p WHERE :idCategory is null OR p.category.id = :idCategory",
            countQuery="SELECT COUNT(p.id) FROM Product p WHERE :idCategory is null OR p.category.id = :idCategory")
    Page<Product> findByNameAndCategory_Id(@Param("idCategory") Long idCategory, Pageable pageable);
}
